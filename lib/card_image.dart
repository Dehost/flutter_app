import 'package:flutter/material.dart';
import 'floating_action_btn.dart';

class CardImage extends StatelessWidget {

  String pathImage = "assets/img/city.jpg";

  CardImage(this.pathImage);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    final card = Container(
      height: 300,
      width: 250,
      margin: EdgeInsets.only(
        top: 60,
        left: 20
      ),

      decoration: BoxDecoration(
        image: DecorationImage(
            fit: BoxFit.cover,
            image: AssetImage(pathImage)
        ),
        borderRadius: BorderRadius.all( Radius.circular(10) ),
        shape: BoxShape.rectangle,
        boxShadow: <BoxShadow> [
          BoxShadow(
            color: Colors.black38,
            blurRadius: 15,
            offset: Offset(0, 7.0)
          )
        ]

      ),
    );

    return Stack(
      alignment: Alignment(0.9, 1.1),
      children: <Widget>[
        card,
        FloatingActionBtn()
      ],
    );
  }

}