import 'package:flutter/material.dart';
import 'card_image.dart';

class CardImageList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container (
      height: 300,
      child: ListView(
        padding: EdgeInsets.all(25),
        scrollDirection: Axis.horizontal,
        children: <Widget>[
          CardImage("assets/img/city.jpg"),
          CardImage("assets/img/navidad.jpg"),
          CardImage("assets/img/paisaje.jpg"),
          CardImage("assets/img/secreto.jpg"),
          CardImage("assets/img/tele.jpg"),
          CardImage("assets/img/image.jpg"),
        ],
      ),
    );
  }

}