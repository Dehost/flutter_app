import 'package:flutter/material.dart';
import 'button_purple.dart';

class DescriptionPlace extends StatelessWidget {

  String namePlace;
  int starts;
  String descriptionPlace;

  DescriptionPlace(this.namePlace, this.starts, this.descriptionPlace);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    final star = Container (
        margin:EdgeInsets.only(
            top:303,
            right:1
        ),
        child: Icon (
            Icons.star,
            color: Colors.amber
        )
    );

    final starHalf = Container (
        margin:EdgeInsets.only(
            top:303,
            right:1
        ),
        child: Icon (
            Icons.star_half,
            color: Colors.amber
        )
    );

    final starBorder = Container (
        margin:EdgeInsets.only(
            top:303,
            right:1
        ),
        child: Icon (
            Icons.star_border,
            color: Colors.amber
        )
    );


    final titleStar = Row (
      children: <Widget>[
        Container(
          margin:  EdgeInsets.only(
            top:300,
            left:20,
            right: 20
          ),//EdgeInsets.only
          child: Text (
            namePlace,
            style: TextStyle (
                fontSize: 30,
                fontFamily: "Lato",
                fontWeight: FontWeight.w900
            ),//TextStyle
            textAlign: TextAlign.left,
          ),//Text
        ),//Container

        Row(
          children: <Widget>[
            star,
            star,
            star,
            starHalf,
            starBorder
          ],
        ),//Row

      ],//children: <Widget>
    );//Container

    final description = Container(
      margin: new EdgeInsets.only(
        top: 20,
        left: 20,
        right: 20
      ),
      child: Text(
        descriptionPlace,
        style: TextStyle(
          fontFamily: "Lato",
          color: Colors.black38
        ),//TextStyle
        textAlign: TextAlign.left
      ),//Text
    );//Container

    return Column(
      children: <Widget>[
        titleStar,
        description,
        ButtonPurple("Navigate")
      ],
    );
  }
}