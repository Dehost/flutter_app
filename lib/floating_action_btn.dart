import 'package:flutter/material.dart';

class FloatingActionBtn extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _FloatingActionButton();
  }

}

class _FloatingActionButton extends State<FloatingActionBtn> {

  bool isPressed = false;
  String textSnackBar = "";

  void onPressedHello () {

    setState((){

      isPressed = isPressed ? false: true;
      textSnackBar = isPressed ? "Removido de favorito." : "Agregado a favorito.";
/*
      Scaffold.of(context).showSnackBar(
          SnackBar(
            content: Text(textSnackBar),
          )
      );
*/
    });

  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return FloatingActionButton(
      backgroundColor: Color(0xFF11DA53),
      foregroundColor: Colors.white,
      mini:true,
      tooltip:"Hello",
      onPressed: onPressedHello,
      child: Icon(
          isPressed ? Icons.favorite : Icons.favorite_border
      )
    );
  }

}