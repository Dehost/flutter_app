import 'package:flutter/material.dart';

import 'review_list.dart';
import 'description_place.dart';
import 'header_appbar.dart';

class Home extends StatelessWidget {

  String descriptionText = "El trozo de texto estándar de Lorem Ipsum usado desde el año 1500 es reproducido debajo para aquellos interesados. Las secciones 1.10.32 y 1.10.33 de 'de Finibus Bonorum et Malorum' por Cicero son también reproducidas en su forma original exacta, acompañadas por versiones en Inglés de la traducción realizada en 1914 por H. Rackham.";

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Stack(
      children: <Widget>[
        ListView(
          children: <Widget>[
            DescriptionPlace("Paula", 4, descriptionText),
            ReviewList()
          ],
        ),
        HeaderAppBar()
      ],
    );
  }
}