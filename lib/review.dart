import 'package:flutter/material.dart';

class Review extends StatelessWidget{

  String pathImage = "assets/img/image.jpg";
  String name = "Diego Tales";
  String info = "1 review 5 photos";
  String comment = "There is an amazing place  un Sri Lanka";

  Review(this.pathImage,this.name,this.info,this.comment);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    final userName = Container(
      margin: EdgeInsets.only(
        left: 20,
      ),
      child: Text(
        name,
        style: TextStyle(
          fontFamily: 'Lato',
          fontSize: 17,
        ),//TextStyle
      ),//Text
    );

    final userInfo = Container(
      margin: EdgeInsets.only(
        left: 20,
      ),
      child: Text(
        info,
        style: TextStyle(
          fontFamily: 'Lato',
          fontSize: 13,
        ),//TextStyle
      ),//Text
    );//Container

    final userComment = Container(
      margin: EdgeInsets.only(
        left: 20,
      ),
      child: Text(
        comment,
        style: TextStyle(
          fontFamily: 'Lato',
          fontSize: 13,
        ),//TextStyle
      ),//Text
    );//Container


    final userDetails = Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
          userName,
          userInfo,
          userComment,
      ],//<Widget>
    );//Column

    final photo = Container(
        margin: EdgeInsets.only(
          top: 20,
          left: 20,
        ),//EdgeInsets.only

        width: 80,
        height: 80,

        decoration: BoxDecoration(
          shape: BoxShape.circle,
          image: DecorationImage(
              image: AssetImage(pathImage),
              fit: BoxFit.cover
          ),//DecorationImage
        ),//BoxDecoration
    );

    return Row(
      children: <Widget>[
        photo,
        userDetails
      ],//<Widget>
    );//Row
  }

}