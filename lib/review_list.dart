import 'package:flutter/material.dart';
import 'review.dart';

class ReviewList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Review("assets/img/image.jpg", "Diego", "4 review 3 photos", "Esto es una prueba"),
        Review("assets/img/image.jpg", "Santiago", "1 review 6 photos", "Esto es una prueba"),
        Review("assets/img/image.jpg", "Ramirez", "3 review 2 photos", "Esto es una prueba"),
        Review("assets/img/image.jpg", "Tales", "1 review 1 photos", "Esto es una prueba"),
        Review("assets/img/image.jpg", "Pascuales", "9 review 6 photos", "Esto es una prueba"),
      ],
    );
  }

}